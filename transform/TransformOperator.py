import numpy as np
import types


class TransformOperator:

    def __init__(self, transform_method, inplace: bool = False):
        self.transform_method = transform_method
        self.inplace = inplace

    def __call__(self, *args, **kwargs):
        assert len(args) == 1
        if isinstance(args[0], types.GeneratorType):
            return self.transform_video(args[0])
        assert type(args[0]) == np.ndarray
        assert len(args[0].shape) == 2 or len(args[0].shape) == 3
        if not self.inplace:
            image = args[0].copy()
        else:
            image = args[0]
        return self.transform_method(image)

    def transform_video(self, video):
        for image in video:
            yield self.__call__(image)
