import cv2
import numpy as np


class TransformList:
    @staticmethod
    def resize(image, scale):
        wd = int(image.shape[1] * scale)
        ht = int(image.shape[0] * scale)
        return cv2.resize(image, (wd, ht))

    @staticmethod
    def normalize(image):
        return (image - image.mean()) / image.std()

    @staticmethod
    def standardise(image):
        mini, maxi = image.min(), image.max()
        return np.clip((image - mini) / (maxi - mini), 0, 1)

    @staticmethod
    def int8(image):
        return np.uint8(255 * TransformList.standardise(image))

    @staticmethod
    def gradient(image, ksize=5):
        dx = cv2.Sobel(image, cv2.CV_64F, 1, 0, ksize=ksize)
        dy = cv2.Sobel(image, cv2.CV_64F, 0, 1, ksize=ksize)
        return np.sqrt(dx ** 2 + dy ** 2)

    @staticmethod
    def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)
        # apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        # return the edged image
        return edged

    @staticmethod
    def laplacian_treshold(image, t):
        lap_mat_abs = np.abs(cv2.Laplacian(image, cv2.CV_64F))
        image[lap_mat_abs <= t] = 255
        image[lap_mat_abs > t] = 0
        return image

    @staticmethod
    def contour(image, filter_contour=None):
        contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        if filter_contour is not None:
            contours = list(filter(lambda x: filter_contour[0] < cv2.contourArea(x) < filter_contour[1], contours))
        image = cv2.drawContours(255 * np.ones(image.shape), contours, -1, (0, 255, 0), 3)
        return image

    @staticmethod
    def hough(image):
        rows = image.shape[0]
        circles = cv2.HoughCircles(image, cv2.HOUGH_GRADIENT, 1, rows / 8,
                                   param1=100, param2=30,
                                   minRadius=10, maxRadius=50)
        print(circles)

        if circles is not None:
            circles = np.uint16(np.around(circles))
            for i in circles[0, :]:
                center = (i[0], i[1])
                # circle center
                cv2.circle(image, center, 1, (0, 100, 100), 3)
                # circle outline
                radius = i[2]
                cv2.circle(image, center, radius, (255, 0, 255), 3)
        return image

    @staticmethod
    def rotate_rect_contour(image):
        contours, hierarchy = cv2.findContours(image, 1, 2)
        rect = cv2.minAreaRect(contours[0])
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        return cv2.drawContours(image, [box], 0, (0, 0, 255), 20)

    @staticmethod
    def clahe(image):
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        return clahe.apply(image)

    @staticmethod
    def erode(image, ksize=5):
        kernel = np.ones((ksize, ksize), np.uint8)
        return cv2.erode(image, kernel, iterations=1)

    @staticmethod
    def dilate(image, ksize=5):
        kernel = cv2.getStructuringElement(cv2.MORPH_DILATE, (ksize, ksize))
        return cv2.dilate(image, kernel)

    @staticmethod
    def opening(image, ksize=5):
        kernel = np.ones((ksize, ksize), np.uint8)
        return cv2.morphologyEx(image, cv2.RETR_EXTERNAL, kernel)

    @staticmethod
    def blob(image):
        params = cv2.SimpleBlobDetector_Params()

        params.minThreshold, params.maxThreshold = 10, 200

        params.filterByArea = True
        params.minArea = 1500
        params.filterByCircularity = True
        params.minCircularity = 0.85

        params.filterByConvexity = True
        params.minConvexity = 0.87

        params.filterByInertia = True
        params.maxInertiaRatio = 0.8

        detector = cv2.SimpleBlobDetector_create(params)

        keypoints = detector.detect(image)
        blobs = cv2.drawKeypoints(image, keypoints, np.array([]), (0, 0, 255),
                                  cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        return blobs

    @staticmethod
    def hog(image):
        winSize = (64, 64)
        blockSize = (16, 16)
        blockStride = (8, 8)
        cellSize = (8, 8)
        nbins = 9
        derivAperture = 1
        winSigma = 4.
        histogramNormType = 0
        L2HysThreshold = 2.0000000000000001e-01
        gammaCorrection = 0
        nlevels = 64
        hog = cv2.HOGDescriptor(winSize, blockSize, blockStride, cellSize, nbins, derivAperture, winSigma,
                                histogramNormType, L2HysThreshold, gammaCorrection, nlevels)
        # compute(img[, winStride[, padding[, locations]]]) -> descriptors
        winStride = (8, 8)
        padding = (8, 8)
        locations = ((10, 20),)
        hist = hog.compute(image, winStride, padding, locations)
        return hist

    @staticmethod
    def morphology_ex(image):
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (20, 20))
        output = cv2.morphologyEx(image, cv2.MORPH_ELLIPSE, kernel)
        return output
