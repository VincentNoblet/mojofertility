from media.MediaLoader import ImageLoader
from media.MediaViewer import *
from utils.quick_view import view_media


class TransformPipeline:

    def __init__(self, transform_list):
        if isinstance(transform_list, list):
            self.transform_list = {str(k): v for k, v in enumerate(transform_list)}
        elif isinstance(transform_list, dict):
            self.transform_list = {"{}_{}".format(i, k): v for i, (k, v) in enumerate(transform_list.items())}
        else:
            raise TypeError("transform_list should be a list or a dict")

    def __call__(self, image, view_option=None, viewer_type=ImageViewer):
        if view_option is not None:
            view_options = ["each", "last"]
            if view_option not in view_options:
                raise ValueError("view_option should be one of the following: {}".format(view_options))
        n = len(self.transform_list)
        if view_option and view_option == "each":
            self.view("Original image", image, viewer_type)
        image_out = image
        for i, (name, transform_method) in enumerate(self.transform_list.items()):
            image_out = transform_method(image_out)
            if view_option and (view_option == "each" or (view_option == "last" and i == n - 1)):
                self.view(name, image_out, viewer_type)
        return image_out

    def view(self, name, image, viewer_type):
        view_media(viewer_type, ImageLoader, name, image)

