from media.MediaLoader import *
from media.MediaViewer import *


def view_media(viewer_type, loader_type, window_name, media):
    viewer_type(window_name=window_name, media_loader=loader_type(media)).view()


def view_image(*args): view_media(ImageViewer, ImageLoader, *args)


def view_video(*args): view_media(VideoViewer, VideoLoader, *args)


def view_image_matplotlib(*args): view_media(MatplotlibViewer, ImageLoader, *args)
