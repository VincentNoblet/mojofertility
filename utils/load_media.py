from pathlib import Path

import cv2

from config import media_default_config


def video_to_generator(video, grey):
    while video.isOpened():
        ret, image = video.read()
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break
        if grey:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        yield image
    video.release()


def sample_generator(gen, step):
    for i, g in enumerate(gen):
        if i % step == 0:
            yield g


def load_video(filename, grey=media_default_config["GREY"]):
    media_path = Path(media_default_config["VIDEO_PATH"]) / (filename + ".avi")
    if not (media_path.exists()):
        raise ValueError("{media_path} does not exist".format(media_path=media_path))
    else:
        return video_to_generator(cv2.VideoCapture(media_path.as_posix()), grey)


def load_image(filename, grey=media_default_config["GREY"]):
    media_path = Path(media_default_config["IMAGE_PATH"]) / (filename + ".png")
    if not (media_path.exists()):
        raise ValueError("{media_path} does not exist".format(media_path=media_path))
    else:
        return cv2.imread(media_path.as_posix(), 1 - grey)
