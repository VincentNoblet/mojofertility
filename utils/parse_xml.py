import xml.etree.ElementTree as Et
import os
from utils.quick_view import view_image
from utils.load_media import load_image
import cv2
from pathlib import Path
from utils.utils import check_dir

def parse_yolo(filepath):
    tree = Et.parse(filepath)
    root = tree.getroot()
    rect = {"0": [], "1": []}

    for o in root:
        if o.tag == "object":
            for b in o:
                if b.tag == "name":
                    my_class = b.text
                if b.tag == "bndbox":
                    my_rect = {anchor.tag: anchor.text for anchor in b}
            rect[my_class].append(my_rect)
    return rect


def save_rect_image(path=Path("data/label/all"), save_path=Path("data/label/")):
    i = 0
    check_dir(save_path / "1")
    check_dir(save_path / "0")
    for filename in os.listdir(path):
        if filename.endswith('.png'):
            name = filename.split(".png")[0]
            xml_path = path / (name + ".xml")
            if xml_path.exists():
                mask = parse_yolo(xml_path.as_posix())
            else: mask = {"0": [], "1": []}
            image = cv2.imread((path / filename).as_posix(), 0)
            for m in mask["1"]:
                ymin, ymax, xmin, xmax = int(m["ymin"]), int(m["ymax"]), int(m["xmin"]), int(m["xmax"])
                rect = image[ymin:ymax, xmin:xmax]
                cv2.imwrite((save_path / "1" / "{}.jpg".format(i)).as_posix(), rect)
                i += 1
            for m in mask["0"]:
                ymin, ymax, xmin, xmax = int(m["ymin"]), int(m["ymax"]), int(m["xmin"]), int(m["xmax"])
                rect = image[ymin:ymax, xmin:xmax]
                cv2.imwrite((save_path / "0" / "{}.jpg".format(i)).as_posix(), rect)
                i += 1

