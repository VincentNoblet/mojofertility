from pathlib import Path


def check_dir(directory: str):
    Path(directory).mkdir(parents=True, exist_ok=True)
