import abc

from utils.load_media import load_image, load_video


class MediaLoader(abc.ABC):
    def __init__(self, source):
        self.source = source

    def get_source(self): return self.source

    @abc.abstractmethod
    def load(self): pass


class OriginImageLoader(MediaLoader):
    def load(self): return load_image(self.source)


class OriginVideoLoader(MediaLoader):
    def load(self): return load_video(self.source)


class ImageLoader(MediaLoader):
    def load(self): return self.source


class VideoLoader(MediaLoader):
    def load(self): return self.source
