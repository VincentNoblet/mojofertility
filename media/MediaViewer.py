import abc

import cv2
import matplotlib.pyplot as plt

from config import media_default_config
from transform.TransformList import TransformList


class MediaViewer(abc.ABC):
    def __init__(self, window_name, media_loader):
        self.media_loader = media_loader
        self.resize_scale = media_default_config["VIEW_RESIZE_RATIO"]
        self.window_name = window_name

    def view(self):
        media = self.media_loader.load()
        self.load_view(media)

    def transform(self, image):
        image_view = image.copy()
        wd = int(image.shape[1] * self.resize_scale)
        ht = int(image.shape[0] * self.resize_scale)
        return cv2.resize(TransformList.standardise(image_view), (wd, ht))

    @abc.abstractmethod
    def load_view(self, media): pass


class VideoViewer(MediaViewer):
    def load_view(self, video):
        for image in video:
            cv2.imshow(self.window_name, self.transform(image))
            key = cv2.waitKey(media_default_config["MSPF"])
            if key == ord(' '):
                cv2.waitKey()
            elif key == ord('q'):
                break
        cv2.destroyAllWindows()


class ImageViewer(MediaViewer):
    def load_view(self, image):
        cv2.imshow(self.window_name, self.transform(image))
        cv2.waitKey()
        cv2.destroyAllWindows()


class MatplotlibViewer(MediaViewer):
    def load_view(self, image):
        plt.figure(dpi=200)
        plt.title(self.window_name)
        if len(image.shape) == 3:
            plt.imshow(self.transform(image)[:, :, ::-1])
        else:
            plt.imshow(self.transform(image)[:, :, ::-1], cmap="gray")
