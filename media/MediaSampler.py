from utils.load_media import sample_generator
import cv2
from pathlib import Path
from config import media_default_config

from utils.utils import check_dir


class MediaSampler:

    def __init__(self, media_loader):
        self.media_loader = media_loader

    def sample(self, step):
        media = self.media_loader.load()
        sample_media = sample_generator(media, step)
        return sample_media

    def save_sample(self, step, label_path=Path(media_default_config["LABEL_PATH"]), sub_path="all"):
        sub_path = label_path / sub_path
        check_dir(sub_path)
        for i, sample in enumerate(self.sample(step)):
            destination = (sub_path / "{filename}.{format}".format(filename=i, format="png")).as_posix()
            write_success = cv2.imwrite(destination, sample)
            if not write_success:
                raise SystemError("Writing samples failed.")
