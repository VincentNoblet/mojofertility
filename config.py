import json
from utils.utils import check_dir

media_default_config = json.load(open('default_media_config.json'))

check_dir(media_default_config["IMAGE_PATH"])
check_dir(media_default_config["VIDEO_PATH"])
check_dir(media_default_config["LABEL_PATH"])
