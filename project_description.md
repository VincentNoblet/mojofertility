# PROJECT DESCRIPTION

Hi, this repository has been developed to tackle the challenge of sperm detections inside a video.
This ReadME is a copy of the notebook project_description.ipynb file that you can run after installing the requirements. It serves the purpose to evaluate my development abilities.
There is another notebook called research_description.ipynb also on the repository's root. This notebook aims to present some ideas I implemented to tackle the problem. It serves the purpose to evaluate my computer vision skills.
As the image and the notebooks are heavy, i put them on another branch  than the master called "presentation". So you should checkout to see it.

Either clone only the master (20Kio):
```sh
$ git clone --single-branch --branch master https://gitlab.com/VincentNoblet/mojofertility.git/
$ cd mojofertility
```

Either clone everything (32 Mio) (advised to make the project run):

```sh
$ git clone https://gitlab.com/VincentNoblet/mojofertility.git/
$ cd mojofertility
$ git checkout presentation
```

# 1 - INSTALLATION
I develop with Pycharm Pro and I am using an Archlinux distribution:
![](utils/readme_image/os.png)

Also, I am using a virtualenv environment created by Pycharm to deal with python package. 
That is why you should have Pycharm or virtualenv installed. My Python version is 3.8.3.

You can install the package locally by running a pip install:

```sh
$ git clone https://gitlab.com/VincentNoblet/mojofertility
$ cd mojofertility
$ git checkout presentation
$ virtualenv venv -p /usr/bin/python3
$ source venv/bin/activate
$ pip install -r requirements.txt
```
If everything went well you should be able to 

# 2 - RUNNING

To run the notebooks on the local environment:
```sh
$ ipython kernel install --user --name=.venv
```

Then run jupyter (it is already installed in the virtualenv environment)
```sh
$ source venv/bin/activate
$ jupyter notebook
```

Click on the link where jupyter is running and select the .venv environment:

![](utils/readme_image/jupyter.png)

If there is a problem with jupyter or with the installation itself you can still read this ReadME and see the images but you won't be able to play the videos.

# 3- DEVELOPMENT FEATURES

#### data
The data folder contains our videos, images and labels datas. We gitignore it by adding a gitignore file in each subfolder of data.

#### Package media
In the media package, the interface MediaLoader is implemented to load images and videos.
The media package enables us to load an image directly from a filename using OriginImageLoader
or to load an image from a numpy matrix using ImageLoader. Same thing for the video.

The image must be stored in data/image with .png format: data/image/{filename}.png
The video must be stored in data/video with .avi format: data/video/{filename}.avi

In the media package, after loading, we can show the image or the video using the MediaViewer interface. The MediaLoader object will be injected inside the Media


```python
from media.MediaLoader import *
from media.MediaViewer import *

# "data/video/1.avi" is the first video
# "data/video" is path set in config.json
filename = "1"
video_loader = OriginVideoLoader(filename)
video_viewer = VideoViewer(window_name=filename, media_loader=video_loader)

# Plays the video "data/video/1.avi"
# Press the space bar to pause
# Press any key to resume
# Press q to exit the video
video_viewer.view()
```


```python
from media.MediaLoader import *
from media.MediaViewer import *

# "data/image/1.png" is the first frame when reading the data/video/1.avi
filename = "1"
image_loader = OriginImageLoader(filename)
image_viewer = ImageViewer(window_name=filename, media_loader=image_loader)

# Show the image "data/image/1.png"
# "data/image" is path set in config.json
# Press any key to exit the image
image_viewer.view()
```

The media package has a third functionality which is called MediaSampler. MediaSampler extracts a sample of frames inside a video. We used for image labelisation.



```python
from media.MediaSampler import MediaSampler

# Samples the data/video/1.avi video each 5 frames and save the image in data/label/default
# "data/label" is path set in config.json
MediaSampler(OriginVideoLoader("1")).save_sample(5, sub_path="default")
```

#### Package transform

transform is a package designed to speed up the researches when trying some successive transformations.

TransformList is a module containing a static TransformList class to list every transformation we need.

The module TransformPipeline enables us to easily apply to grey/color images transformations after transformations:


```python
from transform.TransformList import TransformList
from transform.TranformPipeline import TransformPipeline
from utils.load_media import load_image
from media.MediaViewer import *

# You can pass a list of functions
pipe = TransformPipeline([
    lambda x: cv2.GaussianBlur(x, (5, 5), 2),
    lambda x: TransformList.gradient(x, ksize=11),
])
# OR a dictionnary of functions to put the name of the transformation as a title
pipe = TransformPipeline({
    "gaussian": lambda x: cv2.GaussianBlur(x, (5, 5), 2),
    "gradient": lambda x: TransformList.gradient(x, ksize=11),
})
# The TransformPipeline is a callable. Its output is the output of the last transformation.
# You can choose to view each successive transformation's output by setting a view_option "each"
# Or you can choose "last" to view only the ouput of pipe
# Or you can choose None
# You must select a viewer type: Matplotlib or ImageViewer (OpenCV) (default is OpenCV)

pipe(load_image("2", grey=True), view_option="each", viewer_type=MatplotlibViewer)
plt.show()
```

However the pipe object is a transformation of an image. It can not be called on a video.
To accept videos as an input, you must wrap the pipe object under a TransformOperator object:


```python
import cv2

from transform.TransformOperator import TransformOperator
from transform.TranformPipeline import TransformPipeline
from utils.quick_view import view_video
from utils.load_media import load_video

pipe = TransformPipeline({
    "gaussian": lambda x: cv2.GaussianBlur(x, (5, 5), 1.5),
    "canny": lambda x: cv2.Canny(x, 60, 80)
})

output_video = TransformOperator(pipe)(load_video("1"))
view_video("video", output_video)
```

#### Package utils

All package utilities that are not core functionalities of our project can be found in the package utils.
With the load_media module we can load and sample a media.

##### load_media
Thanks to the video_to_generator function, we can convert a cv2.VideoCapture into a python generator object to access the frames of a video using a simple for function.
The load_video function simply capture the video and apply the video_to_generator function.


```python
from utils.load_media import load_video
from utils.quick_view import view_image

for image in load_video("1"):
    view_image("image", image)


```

##### quick_view
Thanks to quick_view we can easily view an image while keeping the rigor of POO and dependency injection we have in this formulation:


```python
#viewer_type(window_name=window_name, media_loader=loader_type(media)).view()
```

##### parse_xml

parse_xml enables us to parse yolo xml format to convert our xml labelisation of little rectangles into a regular binary dataset with little ractangular images ready to use.
