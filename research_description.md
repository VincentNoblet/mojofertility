# RESEARCH PRESENTATION

I would tell that I spent ~15-20h for researches & development and ~5-10h reading articles and watching videos. I consider that a part of this time is also for my personal culture. I must say that I have learned a lot since the beginning of the week.
I won't present every trial I made but let's say that:
First, I tried not to use deep learning for several reasons.

The algorithm can be way quicker.

It saves a lot of memory.

It can save the use of a GPU so it is way cheaper.

Also, it could be a surprising way of technological differentiation.

Second, I tried a YOLO traning for direct sperm detection.

Last, I tried a binary CNN classification before a potential browsing inside the image for detection.

So first let's find an heuristic to detect our sperm.

# 0 - Heuristic detection


```python
from media.MediaViewer import *
from media.MediaLoader import *
from transform.TranformPipeline import TransformPipeline
from transform.TransformList import TransformList
from transform.TransformOperator import TransformOperator
from utils.load_media import load_image, load_video
from utils.quick_view import *
import matplotlib.pyplot as plt
import cv2

#All imports must be run at first
```

Few comments about the image. Each video contains nearly 300 frames. And each frame is an colored image with 2.3 millions of pixels.
The quality of the image is good. The noise and the blur are low. The sperms are quite easy to follow. Though the sperms seem to be diluted the liquid in which they live.

Some sperms are bigger and more diluted than the others. Some sperms are not moving. Some objects are not moving but are not sperms. Maybe air bubble.
We also can see down on the right of the second video a line (it could be an interface between two liquid).

A - GRADIENT AS A SPERM DETECTOR


```python
grey_image = load_image("1", grey=False)
blur_image = cv2.GaussianBlur(grey_image, (11, 11), cv2.BORDER_DEFAULT)

derivative = {
    "sobel_x": lambda x: cv2.Sobel(x, cv2.CV_64F, 1, 0, ksize=5),
    "sobel_y": lambda x: cv2.Sobel(x, cv2.CV_64F, 0, 1, ksize=5),
    "laplacian": lambda x: cv2.Laplacian(x, cv2.CV_64F),
    "gradient": lambda x: TransformList.gradient(x),
    "hsv": lambda x: cv2.cvtColor(x, cv2.COLOR_BGR2HSV)
}

fig = plt.figure(figsize=(6, 3.2))
for i, (name, transform_method) in enumerate(derivative.items()):
    view_image_matplotlib(name, transform_method(blur_image))

plt.show()
```


    <Figure size 432x230.4 with 0 Axes>



![png](output_3_1.png)



![png](output_3_2.png)



![png](output_3_3.png)



![png](output_3_4.png)



![png](output_3_5.png)


The gradient is way more important near the sperm. It could be a good detector. Let's try to see the colored gradient and apply a simple threshold on it.

B - COLOR GRADIENT + THRESHOLD


```python
pipe = TransformPipeline({
    "gaussian": lambda x: cv2.GaussianBlur(x, (5, 5), 2),
    "gradient": lambda x: TransformList.gradient(x),
    "int": lambda x: TransformList.int8(x),
    "threshold": lambda x: cv2.threshold(x, 60, 250,cv2.THRESH_BINARY)[1]
})
pipe(load_image("2"), view_option="each", viewer_type=MatplotlibViewer)
plt.show()

```


![png](output_5_0.png)



![png](output_5_1.png)



![png](output_5_2.png)



![png](output_5_3.png)



![png](output_5_4.png)


The color is not really useful here. Running

C - COLOR GRADIENT + THRESHOLD + CONTOUR


```python
pipe = TransformPipeline({
    "gaussian": lambda x: cv2.GaussianBlur(x, (5, 5), 2),
    "gradient": lambda x: TransformList.gradient(x, ksize=11),
    "int8": lambda x: TransformList.int8(x[:, :, 1]),
    "threshold": lambda x: cv2.threshold(x,40,250,cv2.THRESH_BINARY)[1],
    "contour": lambda x: TransformList.contour(x)
})
pipe(load_image("2", grey=False), view_option="each", viewer_type=MatplotlibViewer)
plt.show()

```


![png](output_7_0.png)



![png](output_7_1.png)



![png](output_7_2.png)



![png](output_7_3.png)



![png](output_7_4.png)



![png](output_7_5.png)


We managed to keep only big objects and we kept all the sperms. But there are still little circles and the big line.
We can remove little and big contours by filtering them by size.


```python
pipe = TransformPipeline({
    "gaussian": lambda x: cv2.GaussianBlur(x, (5, 5), 2),
    "gradient": lambda x: TransformList.gradient(x, ksize=11),
    "int8": lambda x: TransformList.int8(x[:, :, 1]),
    "threshold": lambda x: cv2.threshold(x,40,250,cv2.THRESH_BINARY)[1],
    "contour": lambda x: TransformList.contour(x, filter_contour=(100, 1000))
})
pipe(load_image("2", grey=False), view_option="last", viewer_type=MatplotlibViewer)
plt.show()
```


![png](output_9_0.png)


The results is not so bad. Let's try it on the video.


```python
output_video = TransformOperator(pipe)(load_video("2", grey=False))
view_video("gradient_threshold", output_video)
```

It is cool but I can still see around 4 circles that are not sperm when I have 13-15 sperms which is a huge error.

D - CANNY AS A SPERM DETECTOR

Let's try the famous canny edge detector.


```python
pipe = TransformPipeline({
    "gaussian": lambda x: cv2.GaussianBlur(x, (5, 5), 1.5),
    "canny": lambda x: cv2.Canny(x, 60, 80)
})
pipe(load_image("2", grey=False), view_option="last", viewer_type=MatplotlibViewer)
plt.show()
output_video = TransformOperator(pipe)(load_video("2", grey=True))
view_video("canny", output_video)
```


![png](output_13_0.png)


    Can't receive frame (stream end?). Exiting ...



```python
pipe = TransformPipeline({
    "gaussian": lambda x: cv2.GaussianBlur(x, (5, 5), 1.5),
    "canny": lambda x: cv2.Canny(x, 50, 80),
    "contour": lambda x: TransformList.contour(x, filter_contour=(0, 100))
})
pipe(load_image("1", grey=False), view_option="last", viewer_type=MatplotlibViewer)
plt.show()
output_video = TransformOperator(pipe)(load_video("2", grey=True))
view_video("canny_contour", output_video)
```


![png](output_14_0.png)


Canny is doing really great here. It is more strict but it keeps the head of the sperm almost entirely.
Though we are not finished because we still need to count the remaining object. Applying a convolution on the binary image with a simple operator on a decision-rule could work.

I have tried a lot of successive transformation among CLAHE, erosion, dilation, opening, blob detection, adaptative threshold. Some of them were fine and would diserve more digging.

# 1 - Custom Yolo

I intended to train a custom Yolo model using the python package: imageai.
I labelised a few dozens of images extracted from one video thanks to the python package labelImg.

![](utils/readme_image/label.png)

I used the other video to test the model.

You will find the colab notebook here to read:

https://colab.research.google.com/drive/1xUJIpXWZizsxcOWD7mUOPW98UVaCk4YE?usp=sharing

You can not execute it since it needs the image on my google drive.
I did not manage to make the model work even in training.

![](utils/readme_image/yolo.png)

I am still discovering YOLO on the understanding of what it does and implement it was the first time.
I don't master the training parameters and the requirements in terms of desirable input and dataset. So it is not a proper way to work.
I do think YOLO (or FasterRCNN) could be a good candidate for our problem.
The japanase and indenosian team of DeepSperm showed good results with YOLO and more with DeepSperm at a real-time rate of 50.3 fps (https://arxiv.org/pdf/2003.01395.pdf).
However, the image they used are very populated and the sperms are well separated from the background so it may be not a good comparison.

# 2 - Cnn binary classification

I transformed my yolo labelisation to create a dataset of ~ 120x120 images of sperm and non-sperm to train a CNN.
I begin to work with EfficienNet.

You will find the colab notebook here to read:

https://colab.research.google.com/drive/1lHAl9qDXOX-KR4Q4G6z3GEKvjiMCBju2?usp=sharing

You can not execute also since it needs the image on my google drive.

I reused a previous project from the plant pathology challenge to achieve this task.
The model worked in training but when I applied it on the test set, it overfitted strongly.
Actually I may lack from images. But the main first reason is that the train/valid split is made on an image level. So the same sperms - in a different position - can be on both the train and the valid set if it belongs to several images.
Also my old parameters are not longer relevant.

Maybe here a HOG extraction + SVM may be enough.

I am thinking on lot of method of the state of the art like Haar Cascade head and tail classifiers but my time is running out so I am going to leave it there.

Thanks for reading !
